# Praca Inżynierska  

Deep reinforcement learning - AI checkers bot.  

# Literatura:   
* **Keras Reinforcement Learning Projects by Giuseppe Ciaburro** *[strona o'reilly]*  ~ trochę ogólnikowo opisane
* **Deep Learning with Python** *[Francois Chollet]* ~ Dobra podstawa do implementowania sieci neuronowych w Kerasie
* **Deep Reinforcement Learning Hands-On** *[Maxim Lapan]*  ~ trochę ogólnikowo opisany temat, jest kilka wartościowych rozdziałów
* **Reinforcement learning project: AI Checkers Player** *[C.L. Dubel, J. Brandsema, L. Lefakis, S. Szóstkiewicz] April 20, 2006* ~ praca naukowa o tematyce AI do warcab, krótkie i opisane trudnym językiem  
* **https://arxiv.org/pdf/1509.01549.pdfï¼ˆpage** ~ Projekt z pisania szachów, porównanie różnych metod
* **Reinforcement Learning An Introduction** - second edition *[R.S. Sutton, A.G Barto]* ~ Bardzo szczegółowo opisana metodyka Reinforcement Learning od podstaw
* **https://www.kth.se/social/files/58865ec8f27654607fb6e9a4/PFinnman_MWinberg_dkand16.pdf** ~ deepQ learning zastosowane do gry w backgammon **DO PRZEJRZENIA**

# Repozytoria powiązane z tematem pracy
**https://github.com/SamRagusa/Checkers-Reinforcement-Learning** (Reinforcement learning checkers)  
*  Widoczna komunikacja między AI i modułem gry  
*  Readme z krótkim opisem metodologii 
*  Brak wykorzystania sieci neuronowych

**https://github.com/maurock/snake-ga** (deep reinforcement learning snake)  
* opis działania: https://towardsdatascience.com/how-to-teach-an-ai-to-play-games-deep-reinforcement-learning-28f9b920440a  
# strony WWW
* https://chrislarson1.github.io/blog/2016/05/30/cnn-checkers/ - artykuł z kodem deep reinforcement leanring checkers  
* https://keras.io - główna strona biblioteki  
* https://elitedatascience.com/keras-tutorial-deep-learning-in-python - keras quick tutorial  
* https://towardsdatascience.com/self-learning-ai-agents-part-ii-deep-q-learning-b5ac60c3f47 - metody oparte na reinforcement learning dla samouczącego się AI
* http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.80.6366&rep=rep1&type=pdf - Temporal difference learning for chess


# Schemat działania
1. **Rozdzielenie na dwa moduły- AI oraz moduł gry**
1. **Sieć neuronowa**
    * input sieci neuronowej:
        - 32 bity/neurony odpowiadające za stan gry przed danym ruchem, tak samo dla stany gry po danym ruchu. Łącznie 64 liczby inputu.
        - Parsowanie 8x8 array na 32 liczby (usunięcie niepotrzebnych pól). 0 to puste pole, 1 i -1 pionki białe/czarne, 5 i -5 królowe.
    * drugi poziom sieci neuronowej: 
        - Neurony tutaj to po prostu funkcje inputu, wagi połączeń między pierwszym a drugim poziomem będą stale równe 1 i nie będą ulegały zmianie.
        - Wyciąganie informacji (osobno dla każdego stanu gry) o liczbie pionów, liczbie królowych dla każdej ze ston. Dodatkowo takie informacje jak zajęte rogi, możliwość zbicia przez przeciwnika w następnej turze, itp. [TODO: Wejść w głębiej w mechanikę gry żeby wyciągnąć więcej kluczowych elementów]
    * trzeci poziom sieci neuronowej:
        - Różnica w elementach z drugiego poziomu dla sytuacji przed ruchem i po ruchu. (np. różnica w liczbie pionków, w liczbie królowych itp.).
        - tutaj wagi połączeń z drugim poziomem sieci również będą stałe, to po prostu funkcje liczbowe neuronów z drugiego poziomu.
    * output:
        - "reward"/prawdopodobieństwo tego ruchu. Output jest jeden i połączony ze wszystkimi neuronami z 3 poziomu. Tutaj wagi połączeń będą ulegały zmianie w trakcie uczenia. Algorytm będzie "wartościował" znaczenie każdego neurona z 3 poziomu. 
1. **Metoda wyboru ruchu AI:**  
    * Metoda łatwiejsza - "zachłanna": (*tutaj obowiązkowo możliwość zbicia przez przeciwnika w następnej turze musi być w sieci neuronowej*).
    Dla każdego ruchu z możliwych do wykonania w danej turze obliczamy reward za ten ruch jak powyżej. Następnie wybieramy ten gdzie reward jest najwyższy i go wykonujemy. 
    * Metoda trudniejsza - "przewidująca":
    Podobnie jak metoda zachłanna z tą różnicą, że sprawdzamy wszystkie kombinacje 3 następnych ruchów jakie mogą być,jest ich mniej więcej 20000 - 50000 więc wykonalne dla
    komputera w racjonalnym czasie. (przeciwnika wartościujemy algorytmem MIN-MAX) i liczymy całkowity reward po tych 3 ruchach, 
    następnie wybieramy ten ruch gdzie ta wartość jest największa.

Zwycięstwo będzie miało największą nagrodę, zwykle używa się największej liczby integer i najmniejszej dla określenia nagrody za zwycięstwo/porażkę. Remis będzie miał wartość 0.


1. ** Metoda uczenia się AI:**
    Tzw. Deep Q learning czyli zastąpienie Qtable przez sieć neronową w metodzie Q learning. Będzie to działać mniej więcej tak:
    Wagi w sieci neuronowej będą na początku zrandomizowane (małe wartości). Agent na początku będzie wybierał ruchy z wykorzystaniem tzw. epsilon greedy policy (wybiera optymalny ruch z
    prawdopodobieństwem epsilon, oraz losowy ruch z możliwych z prawdopodobieństwem 1 - epsilon). Epsilon z czasem będzie powoli dążył do 1. Następnie, z wykorzystaniem algorytmu, który w pewnym 
    uproszczeniu wyglądał będzie tak jak dołączona do repozytorium ilustracja będzie "ulepszał" sieć neuronową.



1. **Komunikacja między AI a modułem gry** (zarys):
    * AI -> Gra:  
    `Make a move()` -> przesyła planszę po ruchu AI, najprawdopodobniej array 8x8 liczb jak opisałem wyżej.  
     AI będzie miało w sobie metody do parsowania planszy 8x8 na 32 liczby i z 32 liczb na array 8x8. AI będzie miało także w sobie metodę do generowania wszystkich możliwych ruchów dla danego stanu planszy gry i danego gracza.
    * Gra -> AI:  
    `SendBoard()` -> wysyła array 8x8 planszy oraz informację czyja jest teraz tura.

Tych metod będzie z pewnością więcej, te będą tymi najważniejszymi w komunikacji.